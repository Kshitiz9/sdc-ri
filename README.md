# SDC Reference Implementation

## Summary

SDCri is a set of Java libraries that implements a network communication framework conforming with the IEEE 11073 SDC specifications

- IEEE 11073-20702: Point-of-care medical device communication Part 20702: Medical Devices Communication Profile for Web Services
- IEEE 11073-10207: Point-of-care medical device communication Part 10207: Domain Information and Service Model for Service-Oriented Point-of-Care Medical Device Communication
- IEEE 11073-20701: Point-of-care medical device communication Part 20701: Service-Oriented Medical Device Exchange Architecture and Protocol Binding

The SDC standard meets increasing demand for medical device interoperability in clinical environments like operating theatres or intensive care units.

The long-term goal of SDCri is to deliver a reference implementation in order to facilitate conformance testing against other SDC implementations.
It is not intended to be used in clinical trials, clinical studies, or in clinical routine.

## License

SDCri is licensed under the MIT license, see [LICENSE](LICENSE) file.

## Changelog

The changelog is available [here](CHANGELOG.md).