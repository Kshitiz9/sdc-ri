package com.example;

public class ProviderMdibConstants {

    public static final String HANDLE_LOCATIONCONTEXT = "LC.mds0";
    public static final String HANDLE_PATIENTCONTEXT = "PC.mds0";

    public static final String HANDLE_ALERT_SIGNAL = "as0.mds0";
    public static final String HANDLE_ALERT_CONDITION = "ac0.mds0";

    public static final String HANDLE_NUMERIC_DYNAMIC = "numeric.ch1.vmd0";
    public static final String HANDLE_ENUM_DYNAMIC = "enumstring2.ch0.vmd0";
    public static final String HANDLE_STRING_DYNAMIC = "string2.ch0.vmd1";
    public static final String HANDLE_WAVEFORM = "rtsa.ch0.vmd0";

    public static final String HANDLE_ACTIVATE = "actop.vmd1_sco_0";

    public static final String HANDLE_SET_VALUE = "numeric.ch0.vmd1_sco_0";
    public static final String HANDLE_SET_STRING_ENUM = "enumstring.ch0.vmd1_sco_0";
    public static final String HANDLE_SET_STRING = "string.ch0.vmd1_sco_0";
    public static final String HANDLE_NUMERIC_SETTABLE = "numeric.ch0.vmd1";
    public static final String HANDLE_ENUM_SETTABLE = "enumstring.ch0.vmd1";
    public static final String HANDLE_STRING_SETTABLE = "string.ch0.vmd1";

}
