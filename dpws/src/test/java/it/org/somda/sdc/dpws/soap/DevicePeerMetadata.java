package it.org.somda.sdc.dpws.soap;

import it.org.somda.sdc.dpws.TestServiceMetadata;

public class DevicePeerMetadata extends TestServiceMetadata {
    public static final String SERVICE_ID_1 = "TestServiceId1";
    public static final String SERVICE_ID_2 = "TestServiceId2";
}
