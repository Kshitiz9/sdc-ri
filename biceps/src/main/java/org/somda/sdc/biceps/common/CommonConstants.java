package org.somda.sdc.biceps.common;

/**
 * Common BICEPS constants.
 */
public class CommonConstants {
    /**
     * Namespace string of the BICEPS Extension Model.
     */
    public static final String NAMESPACE_EXTENSION = "http://standards.ieee.org/downloads/11073/11073-10207-2017/extension";

    /**
     * Namespace string of the BICEPS Participant Model.
     */
    public static final String NAMESPACE_PARTICIPANT = "http://standards.ieee.org/downloads/11073/11073-10207-2017/participant";

    /**
     * Namespace string of the BICEPS Message Model.
     */
    public static final String NAMESPACE_MESSAGE = "http://standards.ieee.org/downloads/11073/11073-10207-2017/message";
}
